#include <stdio.h>

char lower(char c) {
    return (c >= 'A' && c <= 'Z') ? c - 'A' + 'a' : c;
}

int main(int argc, char *argv[])
{
    char c = 'A';
    /* getchar() 与 getc(stdin) 等价 可被实现为宏; fgetc(fp) 函数
     * putchar(c) 与 putc(c, stdout) 等价 可被实现为宏; fputc(c, fp) 函数
     * 每次一行 fgets gets, fputs puts
     */
    /* while ((c = fgetc(stdin)) != EOF) */
    printf("%c, lower:%c\n", c, lower(c));

    int i = 0, a[3] = {4,4,4}, j = 0;
    a[i] = i++;
    for (j = 0; j < 3; j++)
        printf("%d, a[%d]\n", j, a[j]);
    printf("++i 2*i: %d %d\n", ++i, 2 * i);
    return 0;
}
