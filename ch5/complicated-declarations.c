#include <stdio.h>

static char a[5] = {'a', 'b', 'c', 'd', '\0'};
#define ARRPTR 0
#if ARRPTR
char *funcc()
#else
char (*funcc())[]
#endif
{
#if ARRPTR
    return a;
#else
    return &a;
#endif
}

void check()
{
    char (*(*x[3])())[5];
    x[0] = funcc;
#if ARRPTR
    char *c;
    c = x[0]();
    char *p;
    printf("%s\n", c);
    p = &c[2];
    printf("%c\n", *p);
#else
    char (*c)[];
    c = x[0]();
    char p;
    p = (*c)[3];
    printf("%s\n", *c);
    printf("%c\n", p);
#endif
}

int main(void)
{
    check();
    return 0;
}
