#include <stdio.h>

#define MAX 1000
#define NUM 0
static int stack[MAX] = {0};
static int stack_p = 0;

int getop(char *str)
{
    if (!isdigit(*str) && *(str+1) == '\0')
        return *str;
    else
        return NUM;
}
void push(int i)
{
    stack[stack_p++] = i;
}
int pop(void)
{
    return stack[--stack_p];
}

/* 当 '*' 作为参数时需要用引号扩起来
 * 不然会被 shell 扩展为当前目录下的所有文件
 */
int main(int argc, char *argv[])
{
    char *p;
    char type;
    while (--argc > 0) {
        type = getop(*++argv);
        switch (type) {
            case NUM :
                push(atoi(*argv));
                break;
            case '+' :
                push(pop() + pop());
                break;
            case '*' :
                push(pop() * pop());
                break;
        }
    }
    printf("result is: %d\n", pop());
    return 0;
}
