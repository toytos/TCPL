#include <stdio.h>

int strend(char *s, char *t)
{
    // /*
    char *sd = s;
    char *td = t;
    for (; *sd; sd++) ;
    for (; *td; td++) ;
    for (; *sd == *td && sd > s && td > t; sd--, td--) ;
    if (td == t && *sd == *td)
        return 1;
    return 0;
    // */
    /*
    int slen = 0, tlen = 0;
    char *p = s, *q = t;
    for (; *p; p++, slen++) ;
    for (; *q; q++, tlen++) ;
    if (slen >= tlen) {
        p -= tlen;
        q -= tlen;
        while (*p++ == *q++ && *q != '\0') ;
        if (q == t + tlen)
            return 1;
    }
    return 0;
    */
}

int main(int argc, char *argv[])
{
    char *s = "0123456789abcdef";
    char *t = "abcdef";
    int ret = strend(s, t);
    printf("s is end of t ? %d\n", ret);
    return 0;
}
