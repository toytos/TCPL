#include <stdio.h>

char *strcat(char *s, char *t)
{
    char *p = s;
    /* for (; *p; p++) ; */
    if (*p)
        while (*++p) ;
    while (*p++ = *t++) ;
    return s;
}

int main(int argc, char *argv[])
{
    char s[60] = "aabb";
    char *t = "1122";
    strcat(s, t);
    printf("%s\n", s);
    return 0;
}
