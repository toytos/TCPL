#include <stdio.h>
#include <ctype.h>
#include "../5-1/buf.h"

int getfloat(double *f)
{
  int c, sign, next;
  double pow;
  while (isspace(c = getch())) ;
  if (!isdigit(c) && c != EOF && c != '+' && c != '-' && c !='.') {
    ungetch(c);
    return 0;
  }

  sign = c == '-' ? -1 : 1;

  if (c == '+' || c == '-') {
    next = getch();
    if (!isdigit(next)) {
      ungetch(c);
      ungetch(next);
      return 0;
    }
    c = next;
  }

  for (*f = 0.0; isdigit(c); c = getch())
    *f = *f * 10 + (c - '0');

  if (c == '.')
    c = getch();

  for (pow = 1.0; isdigit(c); c = getch()) {
    *f = *f * 10 + (c - '0');
    pow *= 10;
  }
  *f /= pow;
  *f *= sign;

  if (c != EOF)
    ungetch(c);
  return c;
}

int main(int argc, char *argv[])
{
  double f;
  int ret;
  while ((ret = getfloat(&f)) != EOF) {
    if (ret > 0)
      printf("1. double = %g, ret = %d\n", f, ret);
    else
      printf("remove useless char %c\n", getch());
  }
  return 0;
}
