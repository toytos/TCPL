#include <stdio.h>

char *strncpy(char *s, char *t, int n)
{
    char *p = s;
    int i = n;
    for (i = n; *t != '\0' && i > 0; i--)
        *p++ = *t++;
    for (; i > 0; i--)
        *p++ = '\0';
    return s;
}

int main(int argc, char *argv[])
{
    char s[20] = "abcdefghigk";
    char t[] = "123";
    int i = 0;
    strncpy(s, t, 3);
    printf("cpy3 %s\n", s);
    strncpy(s, t, 8);
    printf("cpy8:\n", s[i]);
    for (; i <=20; i++)
        printf("%x ", s[i]);
    return 0;
}
