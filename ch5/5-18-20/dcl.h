#ifndef __DCL_H__
#define __DCL_H__

#define MAXTOKEN 100

enum { NAME, PARENS, BRACKETS };
extern int tokentype;
extern char token[];
extern char name[];
extern char datatype[];
extern char out[];

int gettoken(void);
void dcl(void);
void dirdcl(void);

#endif // __DCL_H__
