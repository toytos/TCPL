```
char **argv
	argv: pointer to pointer to char
              指向 char型指针 的指针
int (*daytab)[13]
	daytab: pointer to array[13] of int
               指向包含13个int型元素数组的指针
int *daytab[13]
	daytab: array[13] of pointer to int
		包含13个指向int型指针元素的数组
void *comp()
	comp: function returning pointer to void
		返回void型指针的函数
void (*comp)()
	comp: pointer to function returning void
		指向返回值为void的函数的指针
char (*(*x())[])()
	x: function returning pointer to array[] of
		pointer to function returning char
		返回值为(指向(返回值为char型的函数指针的)数组的)指针的函数
                        char func()   char func()   char func()
                             ^             ^            ^
        x reutrn p -> |      p      |      p      |     p     |...

char (*(*x[3])())[5]
	x: array[3] of pointer to function returning
		pointer to array[5] of char
		包含3个((返回值为(5个char型元素数组的)指针的)函数指针)元素的数组
            char (*func())[5] char (*func())[5] char (*func())[5]  (返回数组的指针)
                    ^                 ^                 ^
        x: |        p        |        p        |        p        |

```
