#include <stdio.h>
#include <ctype.h>
#include "buf.h"

int getint(int *pn)
{
   int c, sign, next;
   while (isspace(c = getch())) ;

   if (!isdigit(c) && c != EOF && c != '+' && c != '-') {
      ungetch(c);
      return 0;
   }

   sign = c == '-' ? -1 : 1;

   if (c == '+' || c == '-') {
      next = getch();
      if (!isdigit(next)) {
         ungetch(c);
         ungetch(next);
         return 0;
      }
      c = next;
   }

   for (*pn = 0; isdigit(c); c = getch())
      *pn = *pn * 10 + (c - '0');
   *pn *= sign;

   if (c != EOF)
      ungetch(c);
   return c;
}

int main(int argc, char *argv[])
{
   int n, ret;
   while ((ret = getint(&n)) != EOF) {
      if (ret > 0)
         printf("1. int = %d, ret = %d\n", n, ret);
      else
         printf("remove useless char %c\n", getch());
   }
   return 0;
}
