#include <stdio.h>
#include <stdlib.h>

#define MAX_NAME 32
#define MAX_LINE 80
struct group {
  char *g_name;
  char *variable;
};

struct group_list {
  struct group_list *next;
  struct group group;
};

void get_variables();

int main(int argc, char *argv[])
{
  get_variables();
  return 0;
}

void get_variables()
{
  /* char temp[MAX_NAME]; */
  /* char line[MAX_LINE]; */
  char *line = NULL;
  size_t len = 0;
  ssize_t ret = 0;
  FILE *fp = fopen("../ch5/complicated-declarations.c", "r");
  if (fp == NULL) {
    printf("open error\n");
    return ;
  }
  while ((ret = getline(&line, &len, fp)) != -1)
    printf("ret: %d, line: %s, len:%u\n", ret, line, len);
  fclose(fp);
}
