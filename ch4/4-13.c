#include <stdio.h>
#include <string.h>

/*
 * for ?
void reverse(char s[], int start, int end)
{
   if (start >= end)
      return;
   char c;
   c = s[start];
   s[start] = s[end];
   s[end] = c;
   return reverse(s, start + 1, end - 1);
}

void reverse(char s[], char rs[])
{
   if (s >= rs)
      return;
   char c = *s;
   *s = *rs;
   *rs = c;
   reverse(++s, --rs);
}
*/

void _reverse(char s[], int *idx, int *p)
{
   char c = s[(*idx)++];
   if (c != '\0') {
      _reverse(s, idx, p);
      s[(*p)++] = c;
   }
}

void reverse(char s[])
{
   int idx = 0;
   int p = 0;
   _reverse(s, &idx, &p);
}

int main(int argc, char *argv[])
{
   char hello[] = "hello world!";
   /* reverse(hello, 0, strlen(hello) - 1); */
   /* reverse(hello, &hello[strlen(hello)-1]); */
   reverse(hello);
   printf("%s\n", hello);
   return 0;
}
