#ifndef __CALC_H__
#define __CALC_H__

#define MAXOP 100

typedef enum {
  ERR = -1,
  NUMBER,
  PLUS,
  MINUS,
  MULTI,
  DIVIDE,
  MOD,
  SIN,
  EXP,
  POW,
  EQ,
  QUIT,
  SET,
  VAR,
} OP;

typedef enum {
  FALSE = 0,
  TRUE,
} bool;

// error show color
#define ERROR(format, ...) fprintf(stderr, "\033[31m" format "\033[m", ##__VA_ARGS__)

#define NAME_LEN 16
#define VAL_LEN  16

typedef struct variable_pair variables;
struct variable_pair {
  char *name;
  double value;
  variables *next;
};
variables *vars, *var_cur;

/*
typedef struct {
  var *cur;
  var_list *next;
} var_list;

var_list vars_top = {NULL, NULL};
var_list vars_tail = {NULL, NULL};
*/

int getop(char *);
void push(double);
double pop(void);
void show_top_stack(void);
double get_top_stack(void);
int swap_top_stack(void);
int clean_stack(void);
const char *getoperate(OP o);
double get_var(char*);
void set_var(char*, double);
void free_var(void);

extern char name[];
extern double value;

#endif /* __CALC_H__ */
