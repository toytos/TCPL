#include <stdio.h>

#define MIN     0x1 << (sizeof(int) * 8 - 1)
#define N       MIN >> 1
void itoa(int n, char s[])
{
    int i = 0;
    if (n < 0) {
        s[i] = '-';
        n = -n;
    }
    if (n / 10) {
        itoa(n / 10, s);
    }
    for (; i <= 20; i++) {
        if (s[i] == '\0') {
            s[i] = n % 10 + '0';
            break;
        }
    }
}

int main(int argc, char const *argv[])
{
    char s[20] = {0};
    itoa(N, s);
    printf("s:%s\n", s);
    printf("N:%d\n", N);

    return 0;
}
