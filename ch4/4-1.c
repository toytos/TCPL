#include <stdio.h>
#include <string.h>

#define MAX_LINE 1000

int strindex(char s[], char t[])
{
  int i, j, k;
  for (i = 0; '\0' != s[i]; i++) {
    for (j = i, k = 0; s[j] == t[k] && '\0' != t[k]; j++, k++) ;
    if (k > 0 && '\0' == t[k])
      return i;
  }
  return -1;
}

int main(int argc, char *argv[])
{
  char s[] = "Ah Love! could you and I with Fate conspire \
Would not we shatter it to bits -- and then \
Re-mould it nearer to the Heart's Desire!";
  char t[] = "ould";
  int ret = -1;
  ret = strindex(s, t);
  printf("strindex: %d\n", ret);

  int strrindex(char s[], char t[]);
  ret = strrindex(s, t);
  printf("strrindex: %d\n", ret);
  if (ret > 0)
    printf("%s\n", s);
  return 0;
}

int strrindex(char s[], char t[])
{
  int i, j, k;
  for (i = strlen(s); i >= 0; i--) {
    for (j = i, k = 0; s[j] == t[k] && '\0' != t[k]; j++, k++) ;
    if (k > 0 && '\0' == t[k])
      return i;
  }
  return -1;
}
