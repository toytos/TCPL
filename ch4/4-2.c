#include <stdio.h>

#define MAXLINE 100

/* atof: convert string s to double */
double atof(char s[])
{
  double val, val_e;
  int i, sign, sign_e, power = 0, power_e = 0;
  double base;

  for (i = 0; isspace(s[i]); i++)
    ;
  sign = (s[i] == '-') ? -1 : 1;
  if (s[i] == '+' || s[i] == '-')
    i++;
  for (val = 0.0; isdigit(s[i]); i++)
    val = 10.0 * val + (s[i] - '0');
  if (s[i] == '.')
    i++;
  for (power = 0.0; isdigit(s[i]); i++) {
    val = 10.0 * val + (s[i] - '0');
    power--;
  }
  if (s[i] == 'e' || s[i] == 'E')
    i++;
  else
    goto out;
  sign_e = s[i] == '-' ? -1 : 1;
  if (s[i] == '-' || s[i] == '+')
    i++;
  for (power_e = 0; isdigit(s[i]); i++)
    power_e = power_e * 10.0 + (s[i] - '0');
  power += sign_e * power_e;

out:
  if (power > 0) {
    base = 10;
  } else {
    base = 0.1;
    power = -power;
  }
  for (val_e = 1.0; power > 0; power--)
    val_e *= base;
  return sign * val * val_e;
}


int main(int argc, char *argv[])
{
  double sum, atof(char[]);
  char line[MAXLINE];
  int getlines(char[], int);

  sum = 0;
  /* while (getlines(line, MAXLINE) > 0) */
  if (getlines(line, MAXLINE) > 0)
    printf("%g\n", sum += atof(line));
  return 0;
}

int getlines(char s[], int lim)
{
  int i, c;

  i = 0;
  while(--lim > 0 && (c = getchar()) != EOF && c != '\n')
    s[i++] = c;
  if (c == '\n')
    s[i++] = c;
  s[i] = '\0';
  return i;
}
