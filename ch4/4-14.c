#include <stdio.h>

#define swap(t, x, y) {t A = x; x = y; y = A;}
#define dprint(x, y) printf(#x "=%d, " #y "=%d\n", x, y)

#define p_t(n, t, args) n ## t (t args)
void p_t(say, int, i);

void p_t(say, int, i)
{
   printf("%d\n", i);
}

int main(int argc, char *argv[])
{
   int x, y;
   x = 1;
   y = 2;
   swap(int, x, y);
   dprint(x, y);
   sayint(x);
   return 0;
}
