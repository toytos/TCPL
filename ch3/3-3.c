#include <stdio.h>
#include <string.h>

#define MAX 128

void expand(char* s1, char* s2) {
    int i, j;
    char end, temp = 0;
    for (i = j = 0; i < strlen(s1); i++) {
        if (s1[i] == '-' && i > 0 && s1[i+1] != '\0') {
            temp = s1[i-1];
            end = s1[i+1];
            i++;
            for (temp++; temp <= end; temp++, j++)
                s2[j] = temp;
        } else {
            s2[j] = s1[i];
            j++;
        }
    }
}

void expand_r(char* s1, char* s2) {
    if (strlen(s1) <= 0)
        return;
    char temp;
    if (*(s1+1) == '-' && *(s1+2) != '\0') {
        for (temp = *s1; temp < *(s1+2); temp++, s2++) {
            *s2 = temp;
        }
        s1+=2;
    } else {
        *s2++ = *s1++;
    }
    expand_r(s1, s2);
}

int main(int argc, char *argv[])
{
    char* TEST_STR[] = {
        "a-d-z0-3",
        "a-b-c",
        "-a-z"
    };
    int i;
    char s2[MAX] = {0};
    for (i = 0; i < 3; i++) {
        memset(s2, 0, sizeof(s2));
        expand_r(TEST_STR[i], s2);
        printf("s1: %s\n", TEST_STR[i]);
        printf("expand: %s\n", s2);
    }
    return 0;
}
