#include <stdio.h>
#include <string.h>

void reverse(char s[]);

/* itoa: convert n to characters in s */
void itoa(int n, char s[])
{
  int i, sign;
  i = 0;
  if ((sign = n) < 0) {
    /* test:
     * c -17 % 10 return -7
     * perl -17 % 10 return 3
     * not use a negative with % oprator ~
     */
    s[i++] = ((n / 10 * 10) - n) + '0';
    n = -(n / 10);
  }

  for ( ; n > 0; n /= 10) {
    s[i++] = n % 10 + '0';
  }

  if (sign < 0)
    s[i++] = '-';
  s[i] = '\0';
  reverse(s);
}

int main(int argc, char *argv[])
{
  int i = 0;
  int ret = 0;
  int w = sizeof(int) * 8;
  char s[32] = {0};
  printf("pls input a num, e~ will return it :)\n");
  ret = scanf("%d", &i);
  if (!ret > 0) {
    i = -1;
    i &= ~0 << (w-1);
    printf("i &= temp: %x\n", i);
  }
  printf("scanf %d, ret:%d\n", i, ret);
  itoa(i, s);
  printf("main, itoa: %s\n", s);
  return 0;
}

void reverse(char s[])
{
  int i = 0;
  int j = strlen(s) - 1;
  char c;
  for (; i < j; ++i, --j) {
    c = s[i];
    s[i] = s[j];
    s[j] = c;
  }
}
