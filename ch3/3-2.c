#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void escape(char* s, char* t) {
    int i, j;
    for(i = j = 0; i <= strlen(s); i++, j++) {
        switch (s[i]) {
            case '\n': {
                t[j] = '\\';
                t[++j] = 'n';
                break;
            }
            case '\t': {
                t[j] = '\\';
                t[++j] = 't';
                break;
            }
            default:
                t[j] = s[i];
                break;
        }
    }
}

void unescape(char* s, char* t) {
    int i, j;
    for(i = j = 0; i <= strlen(s); i++, j++) {
        if (s[i] == '\\') {
            switch (s[++i]) {
                case 't': {
                    t[j] = '\t';
                    break;
                }
                case 'n': {
                    t[j] = '\n';
                    break;
                }
                default:
                    break;
            }
        } else {
            t[j] = s[i];
        }
    }
}

int main(int argc, char *argv[])
{
    char s[]= "123\t456\n789";
    char t[20] = {0};
    char m[20] = {0};
    printf("===========\n");
    escape(s, t);
    printf("escape: %s\n", t);
    unescape(t, m);
    printf("unescape: %s\n", m);
    return 0;
}
