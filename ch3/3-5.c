#include <stdio.h>
#include <string.h>

void reverse(char s[]);
void itob(int n, char s[], int b)
{
  int i, sign, tmp;
  i = 0;
  if ((sign = n) < 0)
    n = -n;

  do {
    tmp = n % b;
    s[i++] = tmp <= 9 ? tmp + '0' : tmp + 'A' - 10;
  } while ((n /= b) > 0);

  if (sign < 0)
    s[i++] = '-';
  s[i] = '\0';
  reverse(s);
}

int main(int argc, char *argv[])
{
  int i = 124;
  int b = 16;
  char s[32] = {0};
  itob(i, s, b);
  printf("%d, %s base %d\n", i, s, b);
  return 0;
}

void reverse(char s[])
{
  int i = 0;
  int j = strlen(s) - 1;
  char c;
  for (; i < j; ++i, --j) {
    c = s[i];
    s[i] = s[j];
    s[j] = c;
  }
}
