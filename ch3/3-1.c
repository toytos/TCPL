#include <stdio.h>

/* 二分查找 */
int binsearch(int x, int v[], int n) {
    int low, high, mid;
    low = 0;
    high = n - 1;
    while (low < high) {
        mid = (low + high) / 2;
        if (x > v[mid])
            low = mid + 1;
        else
            high = mid;
    }
    if (x == v[low])
        return low;
    return -1;
}

int main(int argc, char *argv[])
{
    int a[4] = {2,3,4,5};
    printf("%d\n", binsearch(5, a, 4));
    return 0;
}
